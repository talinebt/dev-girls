import React, { useState } from 'react';
import PageDefault from '../../../components/PageDefault';
import { Link } from 'react-router-dom';
import FormField from '../../../components/FormField';


function CadastroCategoria() {
  const valoresIniciais = {
    nomeDaCategoria: '',
    descricaoCategoria: '',
    corCategoria: '',
  }
  
  const [categoria, setCategoria] = useState([]);  
  const [values, setValues] = useState(valoresIniciais);



function setValue(chave, valor){
  setValues({
    ...values,
    [chave]: valor,
  });
}

function handleChange(e) {
  const { getAttribute, value} = e.target;
  setValue(
  e.target.getAttribute('name'),
  e.target.value
);
} 

  return (
    <>
      <PageDefault>
        <h1>Cadastro de Categoria: {values.nomeDaCategoria}</h1>
        <form onSubmit={function handlerSubmit(e) {
          e.preventDefault();
          
          setCategoria([
            ...categoria,
            values
          ]);

          setValues(valoresIniciais)
        }}>
          
          <FormField
          label="Nome da categoria"
          type="text"
          value={values.nomeDaCategoria}
          name="nomeDaCategoria"
          onChange={handleChange}
          />
          
          <FormField
          label="Descrição:"
          type="textarea"
          name="descricaoCategoria"
          value={values.descricaoCategoria}
          onChange={handleChange}
        />
         <FormField
          label="Cor"
          type="color"
          name="corCategoria"
          value={values.corCategoria}
          onChange={handleChange}
        />
          <button>
            Cadastrar
              </button>

          <ul>
            {categoria.map((categoriaNova, indice) => {
              return (
                <li key={`${categoria}${indice}`}>
                  {categoriaNova.nomeDaCategoria}
                </li>
              )
            })}
          </ul>
        </form>
        <Link to="/">
          Ir para home
        </Link>
      </PageDefault>
    </>
  )

}
export default CadastroCategoria;